# The Raspberry Pi with LED-Strips
The goal of this was to have some visual feedback while audio is playing.
The script used here searches for the log file of the OSC-Server and every time a
specific message is received from the server it sends a light pulse to the 
LED-Strip.

The script uses threading so if it crashes you have to kill it manually.

## Setting up a LED-Strip

For connecting a LED strip with the Raspberry Pi you need a few things:

- a LED-Strip (we used a strip with 60 LEDs/m)
- a Raspberry Pi
- some connection cables
- a 5V power supply 
- Python installed (see below)
- [ws281x library](https://github.com/jgarff/rpi_ws281x) installed (see below)

## Install the ws281x libary
Go into your project folder and use these commands to install the library:
```bash
sudo apt-get install build-essential python-dev git scons swig
git clone https://github.com/jgarff/rpi_ws281x.git
cd rpi_ws281x
scons
cd python
sudo python setup.py install
```

## Connecting to the Raspberry Pi
Our LED strip has 3 connections points. One for the 5V input, on for ground and one for the data communication.

The ground should be connected with the power supply and the Raspberry Pi (for example you could use pin 6) and the data communication cable with the pin 19.

For using the GPIO 10 pin (pin 19) I recoomend reading the documentation of the [ws281x library](https://github.com/jgarff/rpi_ws281x) carefully. It requires changes in some boot files.

Also on some Pis the SPI communication library is deactivated by default. You can change that by using the command:
```bash
sudo raspi config
```

Then you can follow the instructions in the pictures:

![](https://gitlab.com/miguelfranken90/media-computing-project/-/raw/master/Documentation/led-strip/images/raspi-conf1.png)
![](https://gitlab.com/miguelfranken90/media-computing-project/-/raw/master/Documentation/led-strip/images/raspi-conf2.png)
![](https://gitlab.com/miguelfranken90/media-computing-project/-/raw/master/Documentation/led-strip/images/raspi-conf3.png)


After that you can start testing and playing with your LED-Strip using the `strandtest.py` file located in `rpi_ws281x/python/examples/`.

If you dont see anything, you probably didn't change the PIN variables at the top of the file.

If the LED works, copy the script [`Hardware/Pi/Scripts/live.py`](https://gitlab.com/miguelfranken90/media-computing-project/-/blob/master/Hardware/Pi/Scripts/live.py)
into the `rpi_ws281x/python/examples/` folder so it can use the library.
Start the script with
```bash
sudo python live.py
```
