import time, sched, os.path, logging, threading, os

from neopixel import *
import argparse

# LED strip configuration:
LED_COUNT      = 72      # Number of LED pixels.
LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 255     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

COLOR_ARR = [[0,0,0]]*72
# COLOR_ARR[0] = [255,0,0]
L = len(COLOR_ARR)

s = sched.scheduler(time.time, time.sleep)

OLD_COUNTER = 0
NEW_COLOR = [0,0,0]
COUNTER = 0
INTERRUPT = False


def reducePower(v):
  decay = 0.90
  # if v[0] > v[1] and v[0] > v[2]:
  #   if v[1] > 127:
    
  #   else:

  #   if v[2] > 127:
    
  #   else 
  # if v[1] > v[0] and v[1] > v[2]:
  # else:
  v[0] = int(round(v[0]*decay))
  v[1] = int(round(v[1]*decay))
  v[2] = int(round(v[2]*decay))
  return v

def setColor(strip):
  global OLD_COUNTER
  
  # print "OLD_COUNTER: " + str(OLD_COUNTER)
  # print "COUNTER: " + str(COUNTER)
  # print "COLOR_ARR: " + str(COLOR_ARR)
  # print "NEW_COLOR: " + str(NEW_COLOR) 

  if OLD_COUNTER != COUNTER:
    COLOR_ARR[0] = NEW_COLOR
    OLD_COUNTER = COUNTER

  inc = 0
   

  for i in range(L):
    if inc == 1:
      inc = 0
      continue

    v = COLOR_ARR[i]
    if v != [0,0,0]:
      strip.setPixelColor(i, Color(COLOR_ARR[i][0], COLOR_ARR[i][1], COLOR_ARR[i][2]))

      if i < L - 1:
        if COLOR_ARR[i+1] != [0,0,0]:
          continue
        COLOR_ARR[i+1] = reducePower(v)
        inc = 1
      if i > 1:
        j = i - 2
        COLOR_ARR[j] = [0,0,0]
    else:
      strip.setPixelColor(i, 0)

  if COLOR_ARR[L-3] == [0,0,0] and COLOR_ARR[L-2] != [0,0,0] and COLOR_ARR[L-1] != [0,0,0] :
    COLOR_ARR[L-1] = [0,0,0]
    COLOR_ARR[L-2] = [0,0,0]
   
 
  
  strip.show()
  s.enter(50/1000.0, 1, setColor, (strip,))
  # s.enter(250/1000.0, 1, setColor, (strip,))


def follow(thefile, location):
    thefile.seek(0,2) # Go to the end of the file
    while True:        
        if not os.path.exists(location) or INTERRUPT:
          break
        line = thefile.readline()
        if not line:
            time.sleep(0.1) # Sleep briefly
            continue
        yield line

def thread_file_reading():
  global COUNTER
  global NEW_COLOR
    
  location="/home/pi/Projects/media-computing-project/Server/log/output.log"
  print ("file exist:"+str(os.path.exists(location)))
  f = open(location, "r")
  
  received = ("Received Message").upper()
  
  # colors : [green,red,blue]
  kick = ("/drums/kick").upper()
  kickColor = [255,0,0]
  snare = ('/drums/snare').upper()
  snareColor = [0,0,255]
  hihat = ('/drums/hihat').upper()
  hihatColor = [0,255,0]

  loglines = follow(f, location)
  for line in loglines:
    line_caps = line.upper() 
    if kick in line_caps and received in line_caps:      
      NEW_COLOR = kickColor[:]
      COUNTER += 1
    if snare in line_caps and received in line_caps:
      NEW_COLOR = snareColor[:]
      COUNTER += 1
    if hihat in line_caps and received in line_caps:
      NEW_COLOR = hihatColor[:]
      COUNTER += 1

def main():
  # Process arguments
  parser = argparse.ArgumentParser()
  parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
  args = parser.parse_args()

  # Create NeoPixel object with appropriate configuration.
  strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
  # Intialize the library (must be called once before other functions).
  strip.begin()
  
  logging.info("Main    : before creating thread")
  x1 = threading.Thread(target=thread_file_reading, args=())
  x1.start()
  # x1.join()

  s.enter(500/1000.0, 1, setColor, (strip,))
  s.run()
 
if __name__== "__main__":
  format = "%(asctime)s: %(message)s"
  logging.basicConfig(format=format, level=logging.INFO,
                      datefmt="%H:%M:%S")
  
  try: 
    main()
  except KeyboardInterrupt:
    INTERRUPT=True
